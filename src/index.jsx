const { client, xml, jid } = require("@xmpp/client")
const { EventEmitter } = require('events')

class HoolClient extends EventEmitter {

  constructor(options = {}, ...args) {
    // initial settings go here
    super(...args)

    // example of service: "wss://beta.hool.org:443/xmpp-websocket"

    if (!options.service) {
      console.warn("No XMPP service specified. We won't be able to connect!")
    } else {
      this.service = options.service
    }

    this.xmpp = null
    this.jid = null

    // if there's no resource specified, we'll generate a random ID
    this.resource = options.resource || Math.random().toString(16).substr(2, 8)
  }

  // connect to the xmpp server
  async xmppConnect(myJID, password) {

    // note down the JID somewhere
    this.jid = jid(myJID)

    // TODO: temporarily cache password

    // clear old XMPP object
    if (this.xmpp) {
      await this.xmpp.stop()
    }

    // set XMPP object
    this.xmpp = client({
      service: this.service,
      domain: this.jid.domain,
      resource: this.resource,
      username: this.jid.local,
      password: password,
    })

    // listen if not listening
    this.listen(this.xmpp)
  }

async listen(connection) {

    const { iqCaller } = connection

    connection.on("error", (err) => {
      let error = {}

      // fetch params
      error.code = err.code

      if (err.code == 'ECONNERROR') {
        error.message = 'Connection error. Please try again'
      } else if (err.condition == 'not-authorized') {
        error.code = 'not-authorized'
        error.message = 'Invalid username or password'
      } else{
        error.message = 'Unknown error'
      }

      this.emit('error', error)
    })

    connection.on("offline", () => {
      console.log("offline")
      this.emit('offline', {})
    })

    connection.on("stanza", async (stanza) => {
      console.log(`We got: ${stanza}`)

      let from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined

      // ignore own messages
      if (
        from &&
        from.local == this.jid.local &&
        from.host == this.jid.host
      ) {
        console.log("ignoring own message")
        return
      }

      // handle presence
      if (stanza.is("presence")) {

        // handle tableJoin
        if (stanza.getChildren("game").length) {
          let game = stanza.getChildren("game")[0]

          // check compatibility
          if (game.attrs.xmlns != 'http://jabber.org/protocol/mug') return

          // handle state update
          let status
          if (game.getChildren('status').length) {
            status = game.getChild('status')
          }

          if (status || game.getChildren('state').length) {
            let state = {}

            state.table = stanza.attrs.from

            if (status) state.status = status.text()

            let xData = (game.getChild('state')
              .getChildren('x')
              .find((x) => x.attrs.xmlns == 'jabber:x:data'))

            if (xData) {
              // get hand info
              if (!!xData.getChildren('hands').length) {
                state.hands = []
                xData.getChild('hands').children.forEach((handEl) => {
                  let cards = []
                  let handInfo = []

                  // fetch card data
                  if(handEl.getChild('card')){
                    cards = handEl.children.map((cardEl) => ({
                      rank: cardEl.attrs.rank,
                      suit: cardEl.attrs.suit,
                    }))
                  }

                  // fetch shared info
                  for (let infoType of [
                    'HCP',
                    'pattern',
                    'C',
                    'D',
                    'H',
                    'S',
                  ]) {
                    if (handEl.attrs[infoType]) {
                      handInfo.push({
                        type: infoType,
                        value: handEl.attrs[infoType],
                      })
                    }
                  }

                  state.hands.push({
                    side: handEl.attrs.side,
                    cards: cards,
                    infoShared: handInfo,
                  })
                })
              }

              // get bid info from catchup
              if (!!xData.getChildren('bidinfo').length) {
                state.bidInfo = []
                xData.getChild('bidinfo').children.forEach((bidEl) => {
                  state.bidInfo.push({
                      side: bidEl.attrs.side,
                      type: bidEl.attrs.type,
                      level: bidEl.attrs.level,
                      suit: bidEl.attrs.suit,
                      won: bidEl.attrs.won,
                      round: bidEl.attrs.round
                  })
                })
              }
              // get trick info from catchup
              if (!!xData.getChildren('trickinfo').length) {
                state.tricks = []
                xData.getChild('trickinfo').children.forEach((trickEl) => {
                  let cards = []
                  cards = trickEl.children.map((cardEl) => ({
                    side: cardEl.attrs.side,
                    suit: cardEl.attrs.suit,
                    rank: cardEl.attrs.rank,
                  }))
                  state.tricks.push({
                    cards: cards,
                    winner:trickEl.attrs.winner
                  })
                })
              }
              // get contract info from catchup
              if (!!xData.getChildren('contractinfo').length) {
                let contractinfo = xData.getChild('contractinfo')
                state.contractinfo = {
                  level:contractinfo.attrs.level,
                  suit:contractinfo.attrs.suit,
                  double:contractinfo.attrs.double,
                  redouble:contractinfo.attrs.redouble,
                  declarer:contractinfo.attrs.declarer
                }
              }
              // get table info from catchup
              if (!!xData.getChildren('tableinfo').length) {
                let tableinfo = xData.getChild('tableinfo')
                state.tableinfo = {
                  stage:tableinfo.attrs.stage,
                  turn:tableinfo.attrs.turn,
                  dealer:tableinfo.attrs.dealer,
                  host: tableinfo.attrs.host,
                }
              }
              // get score info
              if (!!xData.getChildren('score').length) {
                state.score = {}
                xData.getChildren('score').forEach((scoreEl) => {
                  let typeScore = []
                  console.log('scoreEl', scoreEl)
                  scoreEl.children.forEach((sideScore) => {
                    typeScore.push({
                      side: sideScore.attrs.side,
                      value: sideScore.text(),
                    })
                  })

                  state.score[scoreEl.attrs.type] = typeScore
                })
              }

              // get undo Info
              if (xData.getChildren('undo').length) {
                let undo = xData.getChild('undo');
                state.undo = {
                  side: undo.attrs.side,
                  mode: undo.attrs.mode
                };
              }

              // get claim Info
              if (xData.getChildren('claim').length) {
                let claim = xData.getChild('claim');
                state.claim = {
                  side: claim.attrs.side,
                  mode: claim.attrs.mode,
                  amount: claim.attrs.amount,
                };
              }

              // get changedeal Info
              if (xData.getChildren('changedeal').length) {
                let changedeal = xData.getChild('changedeal');
                state.changedeal = {
                  type: changedeal.attrs.type,
                  requestee: changedeal.attrs.requestee,
                  status: changedeal.attrs.status
                };
              }

              // get turn info
              if (!!xData.getChildren('turn').length) {
                console.log('turns out', xData.getChildren('turn'))
                state.turn = xData.getChild('turn').attrs.side
              }

              // get bid info
              if (!!xData.getChildren('bidinfo').length) {
                let bids = []
                xData.getChildren('bidinfo').forEach((bidEl) => {
                  bidEl.children.forEach((bid) => {
                    bids.push({
                      round: bid.attrs.round,
                      side: bid.attrs.side,
                      type: bid.attrs.type,
                      level: bid.attrs.level,
                      suit: bid.attrs.suit,
                    })
                  })
                })

                state.bids = bids
              }
            }

            this.emit('stateUpdate', state)
            return
          }

          // carry on with normal presence

          let role
          let jid
          let affiliation

          // check for role info and jid
          if (game.getChildren("item").length) {
            let item = game.getChildren("item")[0]

            role = item.attrs.role
            jid = item.attrs.jid || from.resource
            affiliation = item.attrs.affiliation || 'unknown'
          }

          // handle tableLeave
          if (stanza.attrs.type == 'unavailable') {
              // check if it's permanent (exit)
              if (
                game.getChildren("item").length &&
                game.getChild("item").attrs.affiliation == 'none'
              ) {
                this.emit('tableExit', {
                  table: stanza.attrs.to,
                  user: stanza.attrs.from,
                })
                return
              }

              this.emit('tableLeave', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
              })
              return
            }

          // handle error
          if (stanza.getChildren("error").length) {
            console.log('We have an error')
            let error = stanza.getChild("error")

            this.emit('tableJoinError', {
              type: error.attrs.type || 'unknown',
              tag: error.children[0].name,
              text: error.getChild("text").getText(),
            })
            return
          }

          // broadcast info
          this.emit('tablePresence', {
            table: `${from.local}@${from.domain}`,
            user: `${jid}`,
            role: `${role}`,
            affiliation: affiliation,
          })
          return

        } else if (stanza.attrs.xmlns == 'jabber:client') {
          // handle non-game presences

          if (stanza.attrs.type == 'subscribe') {
            this.emit('roster-subscribe', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'subscribed') {
            this.emit('roster-subscribed', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'unsubscribe') {
            this.emit('roster-unsubscribe', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'unsubscribed') {
            this.emit('roster-unsubscribed', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'unavailable') {
            this.emit('contact-offline', {
              user: stanza.attrs.from,
            })
          } else {
            // extract details
            let show
            let status

            if (stanza.getChildren('show').length) {
              show = stanza.getChild('show').text()
            }

            if (stanza.getChildren('status').length) {
              status = stanza.getChild('status').text()
            }

            this.emit('contact-online', {
              user: stanza.attrs.from,
              show: show,
              status: status,
            })
          }
        }
      }

      // after this, ignore non-messages
      if (!stanza.is("message")) return

      if (stanza.is("message")) {
        // handle turns
        if (stanza.getChildren("turn").length) {
          let turn = stanza.getChild("turn")

          // infoshare
          let infoshare = turn.getChild("infoshare")
          if (
            infoshare &&
            infoshare.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {

            // handle infoshare error
            let error = stanza.getChild("error")
            if (error) {
              this.emit('infoshareError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
                type: infoshare.attrs.type,
                value: infoshare.attrs.value,
                side: infoshare.attrs.side,
                table: infoshare.attrs.table,
              })
              return
            }

            this.emit('infoshare', {
              table: stanza.attrs.to,
              user: stanza.attrs.from,
              side: infoshare.attrs.side,
              type: infoshare.attrs.type,
              value: infoshare.attrs.value,
            })
          }

          // bid
          let bid = turn.getChild("bid")
          if (
            bid &&
            bid.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {

            // handle bid error
            let error = stanza.getChild("error")
            if (error) {
              this.emit('bidError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
                type: bid.attrs.type,
                value: bid.attrs.value,
                side: bid.attrs.side,
                table: bid.attrs.table,
              })
              return
            }

            this.emit('bid', {
              table: stanza.attrs.to,
              user: stanza.attrs.from,
              side: bid.attrs.side,
              type: bid.attrs.type,
              level: bid.attrs.level,
              suit: bid.attrs.suit,
              won: bid.attrs.won,
            })
          }

          // cardplay
          let cardplay = turn.getChild("cardplay")
          if (
            cardplay &&
            cardplay.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {

            // handle cardplay error
            let error = stanza.getChild("error")
            if (error) {
              this.emit('cardplayError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
                rank: cardplay.attrs.rank,
                suit: cardplay.attrs.suit,
                side: cardplay.attrs.side,
                table: cardplay.attrs.table,
              })
              return
            }

            this.emit('cardplay', {
              table: stanza.attrs.to,
              user: stanza.attrs.from,
              side: cardplay.attrs.side,
              rank: cardplay.attrs.rank,
              suit: cardplay.attrs.suit,
            })
          }

          let undo = turn.getChild("undo")
          if (
            undo &&
            undo.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {

            // handle undo error
            let error = stanza.getChild("error")
            if (error) {
              this.emit('undoError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
                type: undo.attrs.type,
                value: undo.attrs.value,
                side: undo.attrs.side,
                table: undo.attrs.table,
              })
              return
            }
            let cards = []
            cards = undo.children.map((cardEl) => ({
              side: cardEl.attrs.side,
              suit: cardEl.attrs.suit,
              rank: cardEl.attrs.rank,
            }))


            this.emit('undo', {
              table: stanza.attrs.table,
              user: stanza.attrs.from,
              mode: undo.attrs.mode,
              side: undo.attrs.side,
              rank: undo.attrs.rank,
              suit: undo.attrs.suit,
              status: undo.attrs.status,
              cards: cards
            })
          }

          let claim = turn.getChild("claim")
          if (
            claim &&
            claim.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {

            // handle claim error
            let error = stanza.getChild("error")
            if (error) {
              this.emit('claimError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
                type: claim.attrs.type,
                value: claim.attrs.value,
                side: claim.attrs.side,
                table: claim.attrs.table,
              })
              return
            }

            this.emit('claim', {
              table: stanza.attrs.table,
              user: stanza.attrs.from,
              mode: claim.attrs.mode,
              side: claim.attrs.side,
              amount: claim.attrs.amount,
            })
          }

          let changedeal = turn.getChild('changedeal');
          if (
          changedeal &&
          changedeal.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {
            // handle changedeal error
            let error = stanza.getChild('error');
            if (error) {
              this.emit('changedealError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild('text').getText(),
                status: changedeal.attrs.status,
                requestee: changedeal.attrs.requestee,
                table: changedeal.attrs.table,
                side: changedeal.attrs.side
              })
              return
            }

            this.emit('changedeal', {
              table: stanza.attrs.table,
              user: stanza.attrs.from,
              type: changedeal.attrs.type,
              status: changedeal.attrs.status,
              requestee: changedeal.attrs.requestee,
              side: changedeal.attrs.side
            });
          }

          //For Scoreboard Handling
          let scoreboard = turn.getChild("scoreboard")
          if (
            scoreboard &&
            scoreboard.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {

            // handle scoreboard error
            let error = stanza.getChild("error")
            if (error) {
              this.emit('scoreboardError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
                type: scoreboard.attrs.type,
                value: scoreboard.attrs.value,
                side: scoreboard.attrs.side,
                table: scoreboard.attrs.table,
              })
              return
            }

            this.emit('scoreboard', {
              table: stanza.attrs.table,
              user: stanza.attrs.from,
              mode: scoreboard.attrs.mode,
              side: scoreboard.attrs.side,
            })
          }

          //For Catchup Handling
          let catchup = turn.getChild("catchup")
          if (
            catchup &&
            catchup.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
          ) {

            // handle catchup error
            let error = stanza.getChild("error")
            if (error) {
              this.emit('catchupError', {
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
                type: catchup.attrs.type,
                value: catchup.attrs.value,
                side: catchup.attrs.side,
                table: catchup.attrs.table,
              })
              return
            }

            this.emit('catchup', {
              table: stanza.attrs.table,
              user: stanza.attrs.from,
              mode: catchup.attrs.mode,
              side: catchup.attrs.side,
            })
          }
        }

        // handle other game-related messages
        if (stanza.getChildren("game").length) {
          let game = stanza.getChild("game")

          // check that it's compatible; if not we silently drop
          if  (game.attrs.xmlns != 'http://jabber.org/protocol/mug#user') {
            console.warn(`Dropping unrecognised stanza: ${stanza}`)
          }

          // invited
          let invited = game.getChild("invited")

          if (
            invited &&
            invited.attrs.var == 'http://hool.org/protocol/mug/hool'
          ) {
            // TODO: reject malformed stanza

            let reason = invited.getChild("reason") || null
            if (reason) reason = reason.text()

            let role = null
            if (game.getChild("item")) {
              role = game.getChild("item").attrs.role || null
            }

            this.emit('invited', {
              invitor: invited.attrs.from,
              invitee: stanza.attrs.to,
              role: role,
              table: stanza.attrs.from,
              reason: reason,
            })
          }

          // declined invite
          let declined = game.getChild("declined")

          if (declined) {
            // TODO: reject malformed stanza

            let reason = declined.getChild("reason") || null
            if (reason) reason = reason.text()

            let role = null
            if (game.getChild("item")) {
              role = game.getChild("item").attrs.role || null
            }

            this.emit('invite-declined', {
              invitee: stanza.attrs.from,
              invitor: declined.attrs.to,
              table: stanza.attrs.from,
              reason: reason,
              role: role,
            })
          }
        }

        // handle chat and groupchat messages
        if (
          stanza.attrs.type == 'groupchat' &&
          stanza.getChildren("body").length
        ) {
          // break up username
          let sender = stanza.attrs.from.split('/')
          this.emit('groupchat', {
             from: stanza.attrs.from,
             to: stanza.attrs.to,
             user: sender[1],
             table: sender[0],
             id: stanza.attrs.id,
             text: stanza.getChild("body").text(),
           })
        } else if (
          stanza.attrs.type == 'chat' &&
          stanza.getChildren("body").length
        ) {
          this.emit('chat', {
            from: stanza.attrs.from,
            to: stanza.attrs.to,
            id: stanza.attrs.id,
            text: stanza.getChild("body").text(),
          })
        }
      }
    })

    connection.on("online", async (address) => {
      console.log("...and we're on!")

      await connection.send(
        <presence>
          <show>chat</show>
          <status>Hi, I'm a test user</status>
        </presence>
      )
      console.log("connected to xmpp server")
      this.emit('connected', {
        status: 'ok',
      })
    })

    console.log('All set up. Trying to connect...')
    connection.start().catch(console.error)
  }

  /**
   * Server-level operations
   *
   * Connecting to a server, disconnecting, service discovery.
   */

  async xmppStop() {
    return await this.xmpp.stop()
  }

  async xmppDiscoverServices() {
    // Discover services
    console.log("discovering services")

    let response = await this.xmpp.iqCaller.request(
      <iq type="get">
        <query xmlns="http://jabber.org/protocol/disco#info"/>
      </iq>
    )

    // check if server supports disco#items
    if (response.getChild('query').children.some((e) => {
        return (
          e.name == 'feature' &&
          e.attrs.var == 'http://jabber.org/protocol/disco#items'
        )
      })
    )
    {
      console.log('disco#items supported')
      // get list of items
      response = await this.xmpp.iqCaller.request(
        <iq type="get">
          <query xmlns="http://jabber.org/protocol/disco#items"/>
        </iq>
      )

      var table_hosts = []

      for await (let service of response.getChild('query').children) {
        // check if it supports hool
        if (service.name == 'item') {

          console.log(`checking: ${service.attrs.jid}`)

          try {

            response = await this.xmpp.iqCaller.request(
              <iq type="get" to={service.attrs.jid}>
                <query xmlns="http://jabber.org/protocol/disco#info"/>
              </iq>
            )

            if (
              response.getChild('query').children.some((feature) => {
                return (
                  feature.name == 'feature' &&
                  feature.attrs.var == 'http://hool.org/protocol/mug/hool'
                )
              })
            ) {
              console.log(`Found Hool service at ${service.attrs.jid}`)
              table_hosts.push(service.attrs.jid)
            }

          } catch(e) {
            console.log(`Warning: skipping ${service.attrs.jid}: ${e}`)
          }
        }

      }


      // give up if we haven't found any hosts
      if (table_hosts.length < 1) {
        console.log("Could not find any table hosts :(")
        return
      }

      console.log(`Found hosts: ${table_hosts}`)

      this.tableService = table_hosts[0]
      return this.tableService

      } else {
        console.log('disco#items not supported here :(')
        return
      }
  }


  /**
   * Roster operations
   *
   * Friend requests, blocking, presence, sending messages.
   */

  rosterSubscribe(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="subscribe"/>
    )
  }

  rosterSubscribed(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="subscribed"/>
    )
  }

 async rosterRemove(userJID){
    let response = await this.xmpp.iqCaller.request(
      <iq type="set">
        <query xmlns="jabber:iq:roster">
          <item jid={userJID} subscription='remove'/>
        </query>
      </iq>
    )
    console.log(response)
  }

  rosterUnsubscribe(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="unsubscribe"/>
    )
  }

  rosterUnsubscribed(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="unsubscribed"/>
    )
  }

  async xmppGetRoster() {
    // fetch roster
    console.log("fetching roster")

    let response = await this.xmpp.iqCaller.request(
      <iq type="get">
        <query xmlns="jabber:iq:roster"/>
      </iq>
    )

    if (response.getChildren('query').length) {
      let roster = []

      for (let contact of response.getChild('query').getChildren('item')) {
        // If both user's has presence supscription then only add them
        roster.push({
          jid: contact.attrs.jid,
          name: contact.attrs.name,
          subscription: contact.attrs.subscription
        })
      }
      
      return roster
    }

    console.log('roster fetched, but no contacts found')
    console.log('make some friends!')
    return []
  }

  /**
   * Table-level operations
   *
   * Listing tables, joining or leaving a table, and so on.
   */

  async getTableList() {

    if (!this.tableService) {
      await this.xmppDiscoverServices()
    }


    // now, get room list
    var response = await this.xmpp.iqCaller.request(
      <iq type="get" to={this.tableService}>
        <query xmlns="http://jabber.org/protocol/disco#items"/>
      </iq>
    )

    var roomList = [] // temporary list with minimal info

    // save in list
    response.getChild("query").children.forEach((r) => {
        roomList.push({
          jid: r.attrs.jid,
          name: r.attrs.name,
          host: r.attrs.hostName,
          time: r.attrs.timeCat,
          kibitzerCount: r.attrs.kibitzerCount,
          playerCount: r.attrs.playerCount
        })
    })

    console.log(`found rooms: ${roomList.map((r)=>{return r.jid})}`)
    return roomList;
  }

  async joinTable(tableID, seat) {
    if (!tableID) throw 'Please specify a tableID'

    // auto-set domain
    if (tableID.indexOf('@') == -1) tableID = `${tableID}@${this.tableService}`

    // seat can be: N, S, E, W, any, none. default: none

    if (!seat) seat = 'none'

    this.xmpp.send(
      <presence from={this.jid} to={tableID}>
        <game var='http://hool.org/protocol/mug/hool'>
          <item role={seat}/>
        </game>
      </presence>
    )
  }

  leaveTable(tableID) {
    if(!tableID) throw 'Please specify a tableID'

    // auto-set domain
    if (tableID.indexOf('@') == -1) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <presence
        from={this.jid}
        to={tableID}
        type='unavailable'>
        <game xmlns='http://jabber.org/protocol/mug'>
          <item affiliation='none' role='none' jid={this.jid}/>
        </game>
      </presence>
    )
  }

  invite(tableID, invitee, seat=null, reason=null) {
		if (!invitee) throw "Please specify an invitee"
		if (!tableID) throw "Please specify the table to which you are inviting!"

    // auto-set domain
    if (tableID.indexOf('@') == -1) tableID = `${tableID}@${this.tableService}`

		this.xmpp.send(
			<message
				from={this.jid}
				to={tableID}
			>
				<game xmlns='http://jabber.org/protocol/mug#user'>
					<invite to={invitee}>
						{reason ?
							<reason>{reason}</reason>
						:
							''
						}
					</invite>
					{seat ?
						<item role={seat}/>
					:
						''
					}
				</game>
			</message>
		)
  }

  decline(tableID, invitor, seat=null, reason=null) {
		if (!invitor) throw "Please specify an invitor"
		if (!tableID) throw "Please specify the table to which you are inviting!"

    // auto-set domain
    if (tableID.indexOf('@') == -1) tableID = `${tableID}@${this.tableService}`

		this.xmpp.send(
			<message
				from={this.jid}
				to={tableID}
			>
				<game xmlns='http://jabber.org/protocol/mug#user'>
					<decline to={invitor}>
						{reason ?
							<reason>{reason}</reason>
						:
							''
						}
					</decline>
					{seat ?
						<item role={seat}/>
					:
						''
					}
				</game>
			</message>
		)
  }

  chat(userJID, text) {
    if (!userJID) throw 'Please specify a user JID!'

    this.xmpp.send(
      <message
        from={this.jid}
        to={userJID}
        type='chat'>
        <body>{text}</body>
      </message>
    )
  }

  groupChat(tableID, text) {
    if (!tableID) throw 'Please specify a tableID'

    // auto-set domain
    if (tableID.indexOf('@') == -1) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='groupchat'>
        <body>{text}</body>
      </message>
    )
  }

  /**
   * Game-level operations
   *
   * Cardplay, scoreboard, and other operations.
   */

  shareInfo(tableID, side, infoType, infoValue) {
    if (!tableID) throw 'Please specify a tableID'
    if (!side) throw 'Please specify the side you are playing for'
    if(!infoType) throw 'Please specify the info type'
    if (['C','D','H','S','HCP','pattern'].indexOf(infoType) == -1) {
      throw `Invalid info type: ${infoType}`
    }
    // infoValue is optional

    // auto-set domain
    if (tableID.indexOf('@') == -1) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <infoshare xmlns='http://hool.org/protocol/mug/hool'
            side={side}
            type={infoType}
            value={infoValue}/>
        </turn>
      </message>
    )
  }

  makeBid(tableID, side, bidType, bidLevel, bidSuit) {
    if (!tableID) throw 'Please specify a tableID'
    if (!side) throw 'Please specify the side you are bidding for'
    if(!bidType) throw 'Please specify the bid type'


    if (['level', 'redouble', 'double', 'pass'].indexOf(bidType) < 0) {
      throw `Invalid bid type: ${bidType}`
    }

    if (bidType == 'level') {
      if (!bidLevel) throw 'Please specify level for level bid'
      if (!bidSuit) throw 'Please specify suit for level bid'
    }

    // auto-set domain
    if (tableID.indexOf('@') < 0) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <bid xmlns='http://hool.org/protocol/mug/hool'
            side={side}
            type={bidType}
            level={bidLevel}
            suit={bidSuit}/>
        </turn>
      </message>
    )
  }

  playCard(tableID, side, rank, suit) {
    if (!tableID) throw 'Please specify a tableID'
    if (!side) throw 'Please specify the side you are playing for'
    if(!rank) throw 'Please specify card rank'
    if (!['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
      .includes(rank)) {
      throw `Invalid rank: ${rank}`
    }
    if (!['C','D','H','S'].includes(suit)) {
      throw `Invalid suit: ${suit}`
    }

    // auto-set domain
    if (tableID.indexOf('@') == -1) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <cardplay xmlns='http://hool.org/protocol/mug/hool'
            side={side}
            rank={rank}
            suit={suit}/>
        </turn>
      </message>
    )
  }

  undo(tableID, mode, data) {
    if (!tableID) throw 'Please specify a tableID'
    if (!mode) throw 'Please specify a undo mode'
    if (!['request', 'accept', 'reject', 'cancel']
      .includes(mode)) {
      throw `Invalid mode: ${mode}`
    }
    if(mode=="request"){
      if (!data.rank) throw 'Please specify the side you are playing for'
      if (!data.side) throw 'Please specify card rank'
      if (!['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
        .includes(data.rank)) {
        throw `Invalid rank: ${data.rank}`
      }
      if (!['C','D','H','S'].includes(data.suit)) {
        throw `Invalid suit: ${data.suit}`
      }
    }

    // auto-set domain
    if (tableID.indexOf('@') < 0) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <undo xmlns='http://hool.org/protocol/mug/hool'
            mode={mode}
            side={data.side?data.side:null}
            rank={data.rank?data.rank:null}
            suit={data.suit?data.suit:null}
          />
        </turn>
      </message>
    )
  }

  changedeal(tableID, side, type) {
    if (!tableID) throw 'Please specify a tableID'
    if (!type) throw 'Please specify a change type'
    if (!side) throw 'Please specify the side you are playing for'
    if (!['redeal', 'replay', 'no']
      .includes(type)) {
      throw `Invalid type: ${type}`
    }

    // auto-set domain
    if (tableID.indexOf('@') < 0) tableID = `${tableID}@${this.tableService}`

    console.log("sender  {}",this.jid)
    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <changedeal xmlns='http://hool.org/protocol/mug/hool'
            side={side}
            type={type}
          />
        </turn>
      </message>
    )
  }

  claim(tableID, mode, data) {
    if (!tableID) throw 'Please specify a tableID'
    if (!mode) throw 'Please specify a undo mode'
    if (!['request', 'accept', 'reject', 'withdraw']
      .includes(mode)) {
      throw `Invalid mode: ${mode}`
    }
    if(mode == 'request'){
      if(!data.side) throw 'Please specify the side you are claim for'
      if(!data.amount) throw 'Please specify the claim Amount'
    }


    // auto-set domain
    if (tableID.indexOf('@') < 0) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <claim xmlns='http://hool.org/protocol/mug/hool'
            mode={mode}
            side={data.side?data.side:null}
            amount={data.amount?data.amount:null}
          />
        </turn>
      </message>
    )
  }

  kickoutUser(tableID,kickoutUser){
    if (!tableID) throw 'Please specify a tableID'
    if (!kickoutUser) throw 'Please specify a kickout user'

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <kickoutuser xmlns='http://hool.org/protocol/mug/hool'
            kickuser={kickoutUser}
          />
        </turn>
      </message>
    )
  }

  scoreboard(tableID, mode, side) {
    if (!tableID) throw 'Please specify a tableID'
    if (!mode) throw 'Please specify a undo mode'
    if (!['ready', 'notready']
      .includes(mode)) {
      throw `Invalid mode: ${mode}`
    }
    if(!side) throw 'Please specify the side you are response for'



    // auto-set domain
    if (tableID.indexOf('@') < 0) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <scoreboard xmlns='http://hool.org/protocol/mug/hool'
            mode={mode}
            side={side}
          />
        </turn>
      </message>
    )
  }

  catchup(tableID, mode, side) {
    if (!tableID) throw 'Please specify a tableID'
    if (!mode) throw 'Please specify a undo mode'
    if (!['catchup']
      .includes(mode)) {
      throw `Invalid mode: ${mode}`
    }
    if(!side) throw 'Please specify the side you are response for'



    // auto-set domain
    if (tableID.indexOf('@') < 0) tableID = `${tableID}@${this.tableService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={tableID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <catchup xmlns='http://hool.org/protocol/mug/hool'
            mode={mode}
            side={side}
          />
        </turn>
      </message>
    )
  }
}

module.exports = {
  HoolClient,
}
